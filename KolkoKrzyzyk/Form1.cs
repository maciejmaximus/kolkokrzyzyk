﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KolkoKrzyzyk
{
    public partial class Form1 : Form
    {
        int[,] buttons = new int[3, 3];
        int player = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void ChangePlayer()
        {
            if (player == 0)
            {
                player += 1;
            }
            else
            {
                player = 0;
            }
        }

        private void SetPicture(Button button)
        {
            if (player == 0)
            {
                button.Image = Properties.Resources.kolko;
            }
            else
            {
                button.Image = Properties.Resources.krzyzyk;
            }
        }
        private void DisableButtons()
        {
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;
        }
        private void EnableButtons()
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
        }

        private void EndGameCheck()
        {
            if ((buttons[0, 0] == buttons[0, 1]) && (buttons[0, 0] == buttons[0, 2]) && (buttons[0, 1] == buttons[0, 2]))
            {
                if (buttons[0, 0] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                else if(buttons[0, 0] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }
            else if ((buttons[1, 0] == buttons[1, 1]) && (buttons[1, 0] == buttons[1, 2]) && (buttons[1, 1] == buttons[1, 2]))
            {
                if (buttons[1, 0] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                if(buttons[1, 0] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }
            else if ((buttons[2, 0] == buttons[2, 1]) && (buttons[2, 0] == buttons[2, 2]) && (buttons[2, 1] == buttons[2, 2]))
            {
                if (buttons[2, 0] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                if (buttons[2, 0] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }

            else if ((buttons[0, 0] == buttons[1, 0]) && (buttons[0, 0] == buttons[2, 0]) && (buttons[1, 0] == buttons[2, 0]))
            {
                if (buttons[0, 0] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                else if (buttons[0, 0] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }
            else if ((buttons[0, 1] == buttons[1, 1]) && (buttons[0, 1] == buttons[2, 1]) && (buttons[1, 1] == buttons[2, 1]))
            {
                if (buttons[0, 1] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                if (buttons[0, 1] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }
            else if ((buttons[0, 2] == buttons[1, 2]) && (buttons[0, 2] == buttons[2, 2]) && (buttons[1, 2] == buttons[2, 2]))
            {
                if (buttons[0, 2] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                if (buttons[0, 2] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }

            else if ((buttons[0, 0] == buttons[1, 1]) && (buttons[0, 0] == buttons[2, 2]) && (buttons[1, 1] == buttons[2, 2]))
            {
                if (buttons[0, 0] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                if (buttons[0, 0] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }
            else if ((buttons[0, 2] == buttons[1, 1]) && (buttons[0, 2] == buttons[2, 0]) && (buttons[1, 1] == buttons[2, 0]))
            {
                if (buttons[0, 2] == 1)
                {
                    lbVerdict.Text = "Player 1 WINS!";
                    DisableButtons();
                }
                if (buttons[0, 2] == 2)
                {
                    lbVerdict.Text = "Player 2 WINS!";
                    DisableButtons();
                }
            }
            else if ((buttons[0, 0] != 0) && (buttons[0, 1] != 0) && (buttons[0, 2] != 0) && (buttons[1, 0] != 0) && (buttons[1, 1] != 0) && (buttons[1, 2] != 0) && (buttons[2, 0] != 0) && (buttons[2, 1] != 0) && (buttons[2, 2] != 0))
            {
                lbVerdict.Text = "REMIS!";
                DisableButtons();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (buttons[0, 0] == 0)
            {
                if (player == 0)
                {
                    buttons[0, 0] = 1;
                }
                else
                {
                    buttons[0, 0] = 2;
                }

                SetPicture(button1);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (buttons[0, 1] == 0)
            {
                if (player == 0)
                {
                    buttons[0, 1] = 1;
                }
                else
                {
                    buttons[0, 1] = 2;
                }

                SetPicture(button2);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (buttons[0, 2] == 0)
            {
                if (player == 0)
                {
                    buttons[0, 2] = 1;
                }
                else
                {
                    buttons[0, 2] = 2;
                }

                SetPicture(button3);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (buttons[1, 0] == 0)
            {
                if (player == 0)
                {
                    buttons[1, 0] = 1;
                }
                else
                {
                    buttons[1, 0] = 2;
                }

                SetPicture(button4);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (buttons[1, 1] == 0)
            {
                if (player == 0)
                {
                    buttons[1, 1] = 1;
                }
                else
                {
                    buttons[1, 1] = 2;
                }

                SetPicture(button5);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (buttons[1, 2] == 0)
            {
                if (player == 0)
                {
                    buttons[1, 2] = 1;
                }
                else
                {
                    buttons[1, 2] = 2;
                }

                SetPicture(button6);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (buttons[2, 0] == 0)
            {
                if (player == 0)
                {
                    buttons[2, 0] = 1;
                }
                else
                {
                    buttons[2, 0] = 2;
                }

                SetPicture(button7);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (buttons[2, 1] == 0)
            {
                if (player == 0)
                {
                    buttons[2, 1] = 1;
                }
                else
                {
                    buttons[2, 1] = 2;
                }

                SetPicture(button8);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (buttons[2, 2] == 0)
            {
                if (player == 0)
                {
                    buttons[2, 2] = 1;
                }
                else
                {
                    buttons[2, 2] = 2;
                }

                SetPicture(button9);
                EndGameCheck();
                ChangePlayer();
            }
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            EnableButtons();
            lbVerdict.Text = "";
            player = 0;

            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    buttons[i, j] = 0;
                }
            }

            button1.Image = null;
            button2.Image = null;
            button3.Image = null;
            button4.Image = null;
            button5.Image = null;
            button6.Image = null;
            button7.Image = null;
            button8.Image = null;
            button9.Image = null;
        }
    }
}
